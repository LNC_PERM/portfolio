(()=>{
  const form = document.querySelector('form');
  const dimInput = document.querySelector('#dimension');
  const ontime = document.querySelector('#ontime');
  const field = document.querySelector('#field');
  const message = document.querySelector('#message');
  const timerBlock = document.querySelector('#timer');
  const DEFAULT_DIM = 4;
  const TIME_MS = 60000;
  const TIME_INTERVAL = 1000;

  const game = {
    dim: DEFAULT_DIM,
    ontime: false,
    timeout: 0,
    timeLeft: TIME_MS / 1000,
    interval: 0,
    cards: [],
    openedCard: false,
    cardsLeft: 0,
    finished: true,
    start: function(){
      this.stopTimer();
      timerBlock.classList.add('hidden');
      this.dim = getDim( DEFAULT_DIM );
      this.cards = [];
      this.openedCard = false;
      createField(this.dim);
      this.finished = false;
      this.ontime = ontime.checked;
      this.ontime && (this.startTimer());
      this.checkStatus();
    },
    checkStatus: function(){
      message.innerHTML = this.cardsLeft ? `Осталось карт: ${this.cardsLeft}` : "Вы выиграли!";
      !this.cardsLeft && this.ontime && this.stopTimer();
    },
    startTimer: function(){
      this.timeLeft = TIME_MS / 1000;
      this.interval = setInterval(() => {
        timerBlock.innerHTML = `Время: ${--this.timeLeft}`;
      }, TIME_INTERVAL);
      this.timeout = setTimeout(()=>{this.endByTimeOut()}, TIME_MS);
      timerBlock.classList.remove('hidden');
    },
    stopTimer: function(){
      clearInterval(this.interval);
      clearTimeout(this.timeout);
      timerBlock.innerHTML = '';
      this.finished = true;
    },
    endByTimeOut: function(){
      this.stopTimer();
      this.cardsLeft && (timerBlock.innerHTML = `Время вышло!`);
    }
  };

  const cardTemplate = {
    id: 0,
    val: 0,
    dom: {},
    opened: false,
    timeout: 0,
    create: function(id){
      this.dom = document.createElement('li');
      this.dom.classList.add('card');
      this.dom.id = id;
      this.id = id;
      this.dom.addEventListener('click', {handleEvent: this.open, card:this});
    },
    setOpen: function(){
      this.dom.innerHTML = this.val;
      this.dom.classList.add('opened');
      this.opened = true;
    },
    open: function(e){
      if (this.card.opened || game.finished){
        return;
      }
      if (game.openedCard === false){//нет открытых карт
        this.card.setOpen();
        game.openedCard = this.card.id;
      }else{
        this.card.setOpen();
        if (game.cards[game.openedCard].val !== this.card.val){
          game.cards[game.openedCard].close();
          this.card.close();
        }else{
          game.cardsLeft -= 2;
          game.checkStatus();
        }
        game.openedCard = false;
      }
    },
    close:function(){
      this.opened = false;
      clearTimeout(this.timeout);
      this.timeout = setTimeout(()=>{
        this.dom.innerHTML = '';
        this.dom.classList.remove('opened');
      }, 500);
    }
  }

  function randomOrder( array ){
    for (let i = array.length - 1; i >= 1; i--){
      const j = Math.round(Math.random() * i);
      const tmp = array[i];
      array[i] = array[j];
      array[j] = tmp;
    }
  }

  function createField(dim){
    const cardsCount = dim * dim;
    const cardNumbers = [];
    for (let i = 1; i <= cardsCount/2; i++){
      cardNumbers.push(i);
      cardNumbers.push(i);
    }
    randomOrder(cardNumbers);

    while (field.hasChildNodes()) {
      field.removeChild(field.firstChild);
    }
    game.cardsLeft = cardsCount;
    for (let i = 0; i < cardsCount; i++){
      let card = {...cardTemplate, val:cardNumbers[i]};
      card.create(i);
      field.append(card.dom);
      game.cards.push(card);
    }
    field.style.gridTemplateRows = `repeat(${dim}, 1fr)`;
    field.style.gridTemplateColumns = `repeat(${dim}, 1fr)`;
    field.style.fontSize = `calc(35vmin/${dim})`;
  }

  function getDim( $default ){
    let val = Number(dimInput.value);
    if ( !((typeof(val)==="number") && !(val % 2) && (val >= 2) && (val <= 10)) ){
      val = $default;
      dimInput.value = val;
    }
    return val;
  }

  form.addEventListener('submit', function(e){
    e.preventDefault();//отменяем стандартное действие браузера
    game.start();
  });

})();

(() => {
  const container = document.querySelector('main');
  let currPage = 1;

  async function loadPosts(page = 1) {
    page || (page = 1);
    currPage = page;
    const response = await fetch(`https://gorest.co.in/public-api/posts?page=${page}`);
    const data = await response.json();
    showPosts(data.data);
    (data.meta.pagination.pages > 1) && showPagination(data.meta.pagination.page, data.meta.pagination.pages);
  }

  function showPosts(posts) {
    const postList = document.createElement('ul');
    for (post of posts) {
      const postItem = document.createElement('li');
      const postLink = document.createElement('a');
      const h2 = document.createElement('h2');
      h2.innerText = post.title;
      const p = document.createElement('p');
      p.innerText = post.body;
      postLink.append(h2);
      postLink.append(p);
      postLink.href = `post.html?id=${post.id}`;
      postLink.classList.add('post-link');
      postItem.append(postLink);
      postItem.classList.add('post-item');
      postList.append(postItem);
    }
    postList.classList.add('post-list');
    container.append(postList);
  }

  function showPagination(currPage, pageCount) {
    const viewCount = 11;
    let pageMin = currPage - Math.floor(viewCount / 2);
    (pageMin < 1) && (pageMin = 1);
    let pageMax = pageMin + viewCount - 1;
    (pageMax > pageCount) && (pageMax = pageCount);

    const ul = document.createElement('ul');
    ul.classList.add('pagination-list');

    if (pageMin !== 1) {
      ul.append(createPaginationElement(1));
      ul.append(createPaginationDots());
    }
    for (let i = pageMin; i <= pageMax; i++) {
      ul.append(createPaginationElement(i));
    }
    if (pageMax !== pageCount) {
      ul.append(createPaginationDots());
      ul.append(createPaginationElement(pageCount));
    }
    container.prepend(ul);
    container.append(ul.cloneNode(true));
  }

  function createPaginationElement(page) {
    const li = document.createElement('li');
    li.classList.add('pagination-item');
    const a = document.createElement('a');
    a.href = `?page=${page}`;
    a.innerText = page;
    a.classList.add('pagination-link');
    (currPage === page) && a.classList.add('current-page');
    li.append(a);
    return li;
  }

  function createPaginationDots() {
    const dots = document.createElement('li');
    dots.classList.add('pagination-dots');
    dots.innerText = '...';
    return dots;
  }

  document.addEventListener("DOMContentLoaded", () => {
    const searchParams = new URLSearchParams(window.location.search);
    const page = parseInt(searchParams.get("page"));
    loadPosts(page);
  });

})();

(() => {
  let postId = 0;
  const container = document.querySelector('main');

  async function loadPost() {
    const response = await fetch(`https://gorest.co.in/public-api/posts/${postId}`);
    const postData = await response.json();
    if (postData.code === 200) {
      showPost(postData.data);
      loadComments();
    } else {
      showError();
    }
  }
  async function loadComments() {
    const response = await fetch(`https://gorest.co.in/public-api/comments?post_id=${postId}`);
    const commentsData = await response.json();
    showComments(commentsData);
  }

  function showPost(postData) {
    const h1 = document.createElement('h1');
    h1.innerText = postData.title;
    const pBody = document.createElement('p');
    pBody.innerText = postData.body;
    pBody.classList.add('post-body');
    const postDate = document.createElement('time');
    postDate.innerText = dateFormat(postData.created_at);
    container.append(h1);
    container.append(postDate);
    container.append(pBody);
  }

  function showError() {
    const h1 = document.createElement('h1');
    h1.innerText = 'Error of data loading';
    container.append(h1);
  }

  function showComments(commentsData) {
    const h2 = document.createElement('h2');
    h2.innerText = 'Comments';
    container.append(h2);
    const commentsList = document.createElement('ul');
    commentsList.classList.add('comments-list');
    for (comment of commentsData.data) {
      const commentElement = document.createElement('li');
      const commentDate = document.createElement('small');
      commentDate.innerText = dateFormat(comment.created_at);
      const commentHeader = document.createElement('h3');
      commentHeader.innerText = comment.name;
      const commentBody = document.createElement('p');
      commentBody.innerText = comment.body;
      commentElement.append(commentDate);
      commentElement.append(commentHeader);
      commentElement.append(commentBody);
      commentElement.classList.add('comments-item');
      commentsList.append(commentElement);
    }
    container.append(commentsList);
  }

  function dateFormat(dateString) {
    const dateCreate = new Date(dateString);
    return ('0' + dateCreate.getDate()).slice(-2)
      + '.' + ('0' + (dateCreate.getMonth() + 1)).slice(-2) + '.' + dateCreate.getFullYear()
      + ' ' + dateCreate.getHours() + ':' + dateCreate.getMinutes();
  }

  document.addEventListener("DOMContentLoaded", () => {
    const searchParams = new URLSearchParams(window.location.search);
    postId = parseInt(searchParams.get("id"));
    loadPost();
  });
})();

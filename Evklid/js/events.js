(() => {

  const workSteps = [
    {
      title: 'Проводим консультацию',
      text: 'Влечет за собой процесс внедрения и модернизации приоритизации разума над эмоциями. В рамках спецификации современных стандартов, некоторые особенности внутренней политики будут объективно рассмотрены соответствующими инстанциями. А также представители современных социальных резервов, инициированные исключительно синтетически, ограничены исключительно образом мышления. Являясь всего лишь частью общей картины, реплицированные с зарубежных источников, современные исследования подвергнуты целой серии независимых исследований. Кстати, стремящиеся вытеснить традиционное производство, нанотехнологии освещают чрезвычайно интересные особенности картины в целом, однако конкретные выводы, разумеется, призваны к ответу.'
    },
    {
      title: 'Составляем смету',
      text: 'Внедрения и модернизации приоритизации разума над эмоциями. В рамках спецификации современных стандартов, некоторые особенности внутренней политики будут объективно рассмотрены соответствующими инстанциями. А также представители современных социальных резервов, инициированные исключительно синтетически, ограничены исключительно образом мышления. Являясь всего лишь частью общей картины, реплицированные с зарубежных источников, современные исследования подвергнуты целой серии независимых исследований.'
    },
    {
      title: 'Привлекаем подрядчиков',
      text: 'Идейные соображения высшего порядка, а также новая модель организационной деятельности требует анализа прогресса профессионального сообщества. Высокий уровень вовлечения представителей целевой аудитории является четким доказательством простого факта: высококачественный прототип будущего проекта напрямую зависит от дальнейших направлений развития. Разнообразный и богатый опыт говорит нам, что новая модель организационной деятельности говорит о возможностях системы массового участия. Принимая во внимание показатели успешности, постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет выполнить важные задания по разработке прогресса профессионального сообщества.'
    },
    {
      title: 'Инспектируем все этапы работ',
      text: 'Высокий уровень вовлечения представителей целевой аудитории является четким доказательством простого факта: высококачественный прототип будущего проекта напрямую зависит от дальнейших направлений развития. Разнообразный и богатый опыт говорит нам, что новая модель организационной деятельности говорит о возможностях системы массового участия. Принимая во внимание показатели успешности, постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет.'
    }
  ];

  document.addEventListener("DOMContentLoaded", () => {
    const swiper = new Swiper('.swiper-container', {
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      },
    });

    const stepBtns = document.querySelectorAll('.workflow__steps-link');
    const [stepTitle, stepText, stepPicture] = [
      document.querySelector('.workflow__subtitle'),
      document.querySelector('.workflow__text'),
      document.querySelector('.workflow__step-bg')
    ];
    stepBtns.forEach(function(currBtn){
      currBtn.addEventListener('click', function(e){
        stepBtns.forEach(function(btn){
          btn.classList.remove('workflow__steps-link_active');
        });
        currBtn.classList.add('workflow__steps-link_active');
        const tabNum = e.currentTarget.dataset.tabnum;
        stepTitle.innerText = workSteps[tabNum].title;
        stepText.innerText = workSteps[tabNum].text;
        stepPicture.style.backgroundImage = `url('img/workflow-${tabNum}.jpg')`;
      });
    });


    const burger = document.querySelector('.burger');

    document.querySelector('.header__burger-btn').addEventListener('click', function(){
      burger.classList.remove('burger_hidden');
      burger.style.height = document.querySelector('.header').offsetHeight + document.querySelector('.hero').offsetHeight + 'px';
    });

    document.querySelector('.burger__close-btn').addEventListener('click', function(){
      burger.style.height = 0;
      burger.classList.add('burger_hidden');
    });

    $(".faq__list").accordion({
      collapsible: true,
      active: false,
      heightStyle: 'content',
      header: '.faq__q-heading'
    });

  });

})();

(() => {
  const main = document.querySelector('main');

  function createInputForm() {
    let form = document.createElement('form');
    let h2 = document.createElement('h2');
    let ul = document.createElement('ul');
    let pMessage = document.createElement('p');
    let button = document.createElement('button');
    const inputElements = [
      { id: 'lastName', type: 'text', title: 'Фамилия' },
      { id: 'firstName', type: 'text', title: 'Имя' },
      { id: 'middleName', type: 'text', title: 'Отчество' },
      { id: 'birthDate', type: 'date', title: 'Дата рождения', min: '1900-01-01' },
      { id: 'yearStart', type: 'number', title: 'Год начала обучения', min: '2000', classList: ['year-input'] },
      { id: 'faculty', type: 'text', title: 'Факультет' },
    ];

    form.id = 'form';
    form.classList.add('form');
    h2.innerHTML = 'Новая запись';
    for (let elem of inputElements) {
      let li = document.createElement('li');
      let input = document.createElement('input');
      let label = document.createElement('label');
      label.for = elem.id;
      label.innerText = elem.title;
      for (prop in elem) {
        input[prop] = elem[prop];
      }
      li.append(label);
      li.append(input);
      li.classList.add('fields-item');
      ul.append(li);
    }

    ul.classList.add('fields-list');
    pMessage.classList.add('message');

    button.classList.add('add-btn');
    button.textContent = 'Добавить';
    button.type = 'submit';

    form.append(h2);
    form.append(ul);
    form.append(pMessage);
    form.append(button);

    return form;
  }

  function createFilters() {
    let filters = document.createElement('div');
    let h2 = document.createElement('h2');
    let ul = document.createElement('ul');
    const inputElements = [
      { id: 'filterNames', type: 'text', title: 'Ф/И/О', classList: ['filter-input wide-input'] },
      { id: 'filterFaculty', type: 'text', title: 'Факультет', classList: ['filter-input wide-input'] },
      { id: 'filterStart', type: 'number', title: 'Год начала обучения', min: '2000', classList: ['filter-input year-input'] },
      { id: 'filterFinish', type: 'number', title: 'Год окончания обучения', min: '2000', classList: ['filter-input year-input'] }
    ];

    filters.classList.add('filters');
    h2.innerHTML = 'Фильтры по таблице';
    for (let elem of inputElements) {
      let li = document.createElement('li');
      let input = document.createElement('input');
      let label = document.createElement('label');
      label.for = elem.id;
      label.innerText = elem.title;
      for (prop in elem) {
        input[prop] = elem[prop];
      }
      li.append(label);
      li.append(input);
      li.classList.add('filters-item');
      ul.append(li);
    }

    ul.classList.add('filters-list');
    filters.append(h2);
    filters.append(ul);

    return filters;
  }

  function createTable() {
    let table = document.createElement('table');
    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');
    const tr = document.createElement('tr');
    const cols = [
      { sortby: 'names', innerText: 'Ф И О' },
      { sortby: 'faculty', innerText: 'Факультет' },
      { sortby: 'birthDate', innerText: 'Дата рождения' },
      { sortby: 'yearStart', innerText: 'Годы обучения' }
    ];
    for (col of cols) {
      const th = document.createElement('th');
      th.classList.add('sortable-th');
      th.dataset.sortby = col.sortby;
      th.innerText = col.innerText;
      tr.append(th);
    }

    table.classList.add('table');
    table.id = 'table';

    thead.append(tr);
    table.append(thead);
    table.append(tbody);

    return table;
  }

  const form = createInputForm();
  const filters = createFilters();
  const table = createTable();
  main.append(form);
  const dataArea = document.createElement('div');
  dataArea.classList.add('data-area');
  dataArea.append(filters);
  dataArea.append(table);
  main.append(dataArea);
  const now = new Date();

  document.querySelector('#birthDate').max = now.getFullYear() + '-'
    + ('0' + (now.getMonth() + 1)).slice(-2) + '-'
    + ('0' + now.getDate()).slice(-2);
  document.querySelector('#yearStart').max = now.getFullYear();
})();

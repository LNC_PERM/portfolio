(() => {
  const form = document.querySelector('#form');
  const table = document.querySelector('#table');
  const message = document.querySelector('.message');
  const filters = document.querySelectorAll('.filter-input');
  const sorters = document.querySelectorAll('.sortable-th');
  const EDUCATION_PERIOD = 4;
  const MKSEC_PER_YEAR = 1000 * 3600 * 24 * 365.25;
  const now = new Date();
  const allStudents = [];
  const student = {
    firstName: '',
    lastName: '',
    middleName: '',
    birthDate: '',
    yearStart: 0,
    faculty: ''
  };

  const formHandler = {
    fields: {},
    values: {},
    init: function () {
      this.fields = { ...student };
      for (let id in student) {
        this.fields[id] = document.querySelector(`#${id}`);
        this.fields[id].addEventListener('input', () => {
          this.fields[id].classList.remove('alert');
          this.hideMassage();
        });
      }
    },
    getValues: function () {
      for (let id in this.fields) {
        this.values[id] = this.fields[id].value.trim();
      }
      this.values['birthDate'] = this.values['birthDate'] ? new Date(this.values['birthDate']) : null;
      this.values['yearStart'] = parseInt(this.values['yearStart']);
    },
    invalidFields: function () {
      const res = [];
      for (let id in this.values) {
        if (!this.values[id]) {
          res.push(id);
          continue;
        }
      }
      return res;
    },
    alertFields: function (fieldIds) {
      for (let fName of fieldIds) {
        this.fields[fName].classList.add('alert');
      }
    },
    clear: function () {
      for (let id in student) {
        this.fields[id].value = student[id];
      }
    },
    showMessage: () => {
      message.innerHTML = 'Заполните выделенные поля!';
    },
    hideMassage: () => {
      message.innerHTML = '';
    }
  };

  function addStudent() {
    formHandler.getValues();
    const invalidFields = formHandler.invalidFields();
    if (invalidFields.length) {
      formHandler.showMessage();
      formHandler.alertFields(invalidFields);
    } else {
      allStudents.push({ ...formHandler.values });
      formHandler.clear();
      showTable(allStudents);
    }

  }

  function showTable(array) {
    const tbody = table.querySelector('tbody');
    tbody.innerHTML = '';
    for (let row of array) {
      const tr = document.createElement('tr');
      let td = document.createElement('td');
      td.innerHTML = row['lastName'] + ' ' + row['firstName'] + ' ' + row['middleName'];
      tr.append(td);
      td = document.createElement('td');
      td.innerHTML = row['faculty'];
      tr.append(td);
      td = document.createElement('td');
      td.innerHTML = ('0' + row['birthDate'].getDate()).slice(-2) + '.'
        + ('0' + (row['birthDate'].getMonth() + 1)).slice(-2) + '.'
        + row['birthDate'].getFullYear()
        + ' (' + Math.floor((now.getTime() - row['birthDate'].getTime()) / MKSEC_PER_YEAR) + ' лет)';
      tr.append(td);
      td = document.createElement('td');
      td.innerHTML = educationPeriodOutput(row['yearStart']);
      tr.append(td);
      tbody.append(tr);
    }
  }

  function educationPeriodOutput(yearStart) {
    const endEducation = new Date((yearStart + EDUCATION_PERIOD) + '-09-01');
    const beginEducation = new Date(yearStart + '-09-01');
    return `${yearStart} - ${yearStart + EDUCATION_PERIOD}
       (${now.getTime() > endEducation.getTime()
        ? 'закончил'
        : (Math.ceil((now.getTime() - beginEducation.getTime()) / MKSEC_PER_YEAR)) + ' курс'})`;
  }

  form.addEventListener('submit', function (e) {
    e.preventDefault();
    addStudent();
  });

  let filtersTimeout;
  for (let f of filters) {
    f.addEventListener('input', () => {
      clearTimeout(filtersTimeout);
      filtersTimeout = setTimeout(filterTable, 500);
    });
  }

  for (let s of sorters) {
    s.addEventListener('click', () => {
      sortBy(s.dataset.sortby);
    });
  }

  function sortBy(sortBy) {
    switch (sortBy) {
      case 'names':
        allStudents.sort((a, b) => {
          if ((a.lastName + a.firstName + a.middleName).toLowerCase() > (b.lastName + b.firstName + b.middleName).toLowerCase()) {
            return 1;
          }
          if ((a.lastName + a.firstName + a.middleName).toLowerCase() < (b.lastName + b.firstName + b.middleName).toLowerCase()) {
            return -1;
          }
        });
        break;
      case 'faculty':
        allStudents.sort((a, b) => {
          if (a.faculty.toLowerCase() > b.faculty.toLowerCase()) {
            return 1;
          }
          if (a.faculty.toLowerCase() < b.faculty.toLowerCase()) {
            return -1;
          }
        });
        break;
      case 'birthDate':
        allStudents.sort((a, b) => {
          return a.birthDate.getTime() - b.birthDate.getTime();
        });
        break;
      case 'yearStart':
        allStudents.sort((a, b) => {
          return a.yearStart - b.yearStart;
        });
        break;
    }

    showTable(allStudents);
  }

  function filterTable() {
    let filterNames = '';
    let filterFaculty = '';
    let filterStart = 0;
    let filterFinish = 0;
    for (let f of filters) {
      f.id === 'filterNames' && (filterNames = f.value.trim().toLowerCase());
      f.id === 'filterFaculty' && (filterFaculty = f.value.trim().toLowerCase());
      f.id === 'filterStart' && (filterStart = Number(f.value));
      f.id === 'filterFinish' && (filterFinish = Number(f.value));
    }
    const filtered = allStudents
      .filter(student => !filterNames || (student.lastName + ' ' + student.firstName + ' ' + student.middleName).toLowerCase().includes(filterNames))
      .filter(student => !filterFaculty || student.faculty.toLowerCase().includes(filterFaculty))
      .filter(student => !filterStart || student.yearStart === filterStart)
      .filter(student => !filterFinish || student.yearStart + EDUCATION_PERIOD === filterFinish);
    showTable(filtered);
  }

  document.addEventListener("DOMContentLoaded", () => {
    formHandler.init();
    showTable(allStudents);
  });

})();
